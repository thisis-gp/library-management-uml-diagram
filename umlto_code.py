class Book:
    def __init__(self, isbn, book_name, author, price, noof_pages, language):
        self.ISBN = isbn
        self.book_name = book_name
        self.author = author
        self.price = price
        self.noof_pages = noof_pages
        self.language = language
        self.status = "locked"

    def get_ISBN(self):
        return self.ISBN

    def get_bookname(self):
        return self.book_name

    def get_author(self):
        return self.author

    def get_price(self):
        return self.price

    def get_nopages(self):
        return self.noof_pages

    def get_language(self):
        return self.language

    def set_status(self, status):
        self.status = status


class Shelf:
    def __init__(self, books=[]):
        self.books = books

    def add_book(self, book):
        self.books.append(book)

    def getno_books(self):
        return len(self.books)


class Department:
    def __init__(self, name, location, shelfs=[]):
        self.shelfs = shelfs
        self.name = name
        self.location = location

    def get_name(self):
        return self.name

    def get_location(self):
        return self.location

    def add_shelf(self, shelf):
        self.shelfs.append(shelf)

    def getno_shelfs(self):
        return len(self.shelfs)

    def getno_books(self):
        shelf = Shelf()
        count = 0
        for i in self.shelfs:
            shelf

class Library():
    def __init__(self, name, address, contact_no, in_charge, departments=[]):
        self.name = name
        self.address = address
        self.contact_no = contact_no
        self.in_charge = in_charge
        self.departments = departments

    def get_name(self):
        return self.name

    def get_address(self):
        return self.address

    def get_contactno(self):
        return self.contact_no

    def get_incharge(self):
        return self.in_charge

    def add_department(self, department):
        self.departments.append(department)

    def getno_departments(self):
        return len(self.departments)


Book_one = Book('Me1', 'Test1')
Book_two = Book('Me2', 'Test2')
Book_three = Book('Me3', 'Test3')
Book_four = Book('Me4', 'Test1')
Book_five = Book('Me5', 'Test2')
Book_six = Book('Me6', 'Test3')

shelf_one = Shelf()
shelf_two = Shelf()
shelf_three = Shelf()
shelf_one.add_book(Book_one)
shelf_two.add_book(Book_two)
shelf_three.add_book(Book_three)
shelf_one.add_book(Book_four)
shelf_two.add_book(Book_five)
shelf_three.add_book(Book_six)

department_one = Department('Horror')
department_two = Department('Anime')
department_one.add_shelf(shelf_one)
department_two.add_shelf(shelf_two)
department_two.add_shelf(shelf_three)

library = Library('Ekm')
library.add_department(department_one)
library.add_department(department_two)
count = 0


